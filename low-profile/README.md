# low profile antenna mod

The reception for this mod is not as good as with the "ears" mod, but it still
beats the default antenna in its default placement. It has the advantage of
being much less prone to breaking. It also leaves the ports on either side of
the laptop completely clear.

To do this mod, two holes need to be drilled in the acrylic bottom plate in line
with the screws and about 1cm toward the middle for the antenna wires to pass
into the case. The four user-side bottom plate screws must also be replaced with
four countersunk M2x10 screws, such as the ones in [this
set](https://www.amazon.com/gp/product/B06Y3M62Z8/).

The STL provided is for the right side. Mirror it to get the left side.

In the photos below, I've replaced two of the clear rubber feet with [these
black ones](https://www.amazon.com/dp/B07G8926LH).

![front view](front-view.jpg)
![underside view](underside-view.jpg)
![underside angle view](underside-angle-view.jpg)
