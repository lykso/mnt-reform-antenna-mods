r=2;
wall=2;
overhang = 4;
width = 140;
height = 22;
depth = 10;

antenna = [110, 2, 21];

module roundedCube(xyz, r=5, center=false, use_cylinder=false, $fn=20) {
  module _corner() {
      if (use_cylinder) cylinder(r=r, h=r, $fn=$fn);
      else sphere(r=r, $fn=$fn);
  }
  translate([r, r, use_cylinder ? 0 : r])
  minkowski() {
    cube([xyz.x - r*2, xyz.y - r*2, max(0.1, xyz.z - (use_cylinder ? r : r*2))]);
    _corner();
  }
}

module screw(length) {
    size = 2.25;
    length = length + 0.25;

    rotate([0, 180, 0])
    union() {
        hull() {
            // slop = 0.1
            translate([0, 0, -4])
                cylinder(d=4.25, h=4, $fn=50);
            translate([0, 0, 1.34])
                cylinder(d=2.25, h=0.01, $fn=50);
        }
        cylinder(h=length, d=size, $fn=100);
    }
}

difference() {
    translate([-r,0,-r])
        roundedCube([width + overhang + r, depth + overhang, height + wall + r], r, $fn=100);


    translate([-r,0,-r])
        cube([r, depth + overhang, height + wall + r]);

    translate([0,0,-r])
        cube([width + overhang, depth + overhang, r]);

    // laptop cutout
    cube([width, depth, height]);

    // antenna cutout
    antenna_pos = [(width - antenna.x) / 2, depth, 0];
    translate(antenna_pos)
        cube(antenna);

    screw_height = height + wall;
    outer_screw = [width - 5, depth - 5, screw_height];
    inner_screw = [width - 99.5, depth - 5, screw_height];

    translate(outer_screw)
        #screw(6);

    translate(inner_screw)
        screw(6);

    wire = [20, 10, 1];
    translate([antenna_pos.x, depth - wire.y + antenna.y, antenna.z])
        #cube([wire.x, wire.y, wire.z + height - antenna.z]);

    bumper = 9;
    translate([outer_screw.x - bumper/2, -bumper/4, height])
        #cylinder(d=bumper, h=wall, $fn=100);
}
