# ears antenna mod

This mod provides the best possible wifi reception for the antennas used, as it
places them well away from the body of the laptop. They tuck away neatly under
the keyboard overhang, adding no width to the laptop when not deployed. The
primary drawbacks are that they are prone to being broken off accidentally and
that the laptop ports are covered when they are tucked away.

A small notch on each acrylic port plate must be filed into the rearmost side of
the rearmost port hole to allow the antenna wires to pass into the body of the
laptop. The two M2x4 screws on the screen-side of the panels will also need to
be replaced with a pair of M2x6 screws. The ears are held in place by the
attraction of the user-side panel screws to the 8x3mm neodymium magnets in the
tips of the ears.

![front view](front-view.jpg)
![outside view](outside-view.jpg)
![inside view](inside-view.jpg)
