/*
Copyright CERN 2020. This source describes Open Hardware and is licensed under the CERN-OHL-S v2.

You may redistribute and modify this documentation and make products using it under the terms of the CERN-OHL-S v2 (https:/cern.ch/cern-ohl).

This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITYAND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions. 
*/

slop = 0.25; // How much extra space to add between parts, where applicable.
spindle_slop = 0.1; // How much extra space to leave around the spindle.

wire = 2;
wire_connector = 4;

screw_stem = 2;
screw_head = 4;
screw_head_height = 2;

magnet_diameter = 8;

/* You probably won't want to change these unless you're making significant design changes.
*/
thickness = 1; // Target wall thickness.
plate = thickness;
antenna = [98, 14.5, 2];
antenna_blade = [antenna.x, antenna.y + thickness*2, antenna.z + thickness];

mount_area = [25, 19.5, 5];
spindle_diameter = screw_head + thickness * 6;
piece_height = mount_area.z - plate;

$fn=100;
max_diameter = min(mount_area.x, mount_area.y);

module plate() {
    difference() {
        union() {
            translate([0, 0, -plate]) {
                hull() {
                    cylinder(h=plate, d=max_diameter);
                    translate([-max_diameter/2, 0, 0])
                    cube([max_diameter, mount_area.y/2, plate]);
                }
            }
        }
        translate([0, 0, -plate])
            cylinder(h=plate, d=screw_stem);
        
        peg = (spindle_diameter/2) / cos(45) + slop;
        
        translate([-peg/2, -peg/2, -plate])
            cube([peg, peg, plate]);
    }
}

module spindle_solid(_slop=0) {
    peg = (spindle_diameter/2) / cos(45);
    
    translate([-peg/2, -peg/2, -plate])
        cube([peg, peg, plate]);
    
    cylinder(d=spindle_diameter + _slop, h=piece_height);
    translate([0, 0, piece_height - thickness - _slop])
        cylinder(d=spindle_diameter + thickness*2 + _slop, h=thickness + _slop);
    
    translate([0, 0, piece_height - thickness*2 - _slop])
        cylinder(h=thickness, d1=spindle_diameter, d2=spindle_diameter + thickness*2);
}

module spindle() {
    difference() {
        spindle_solid();
        translate([0, 0, -plate])
            cylinder(h=mount_area.z, d=screw_stem);
        
        translate([0, 0, piece_height - screw_head_height])
            cylinder(h=screw_head_height, d=screw_head);
    }
    
    ;
}

first_ring_diameter = spindle_diameter + thickness*4 + slop;
module first_ring() {
    difference() {
        cylinder(d=first_ring_diameter, h=piece_height);
        spindle_solid(_slop=spindle_slop);
        
        translate([first_ring_diameter/2 - thickness- slop, -(thickness+slop)/2, 0])
            cube([thickness + slop, thickness + slop, 2]);
        
        translate([-first_ring_diameter/2, -(thickness+slop)/2, 0])
            cube([thickness + slop, thickness + slop, 2]);
        
        translate([-(thickness+slop)/2, first_ring_diameter/2 - thickness- slop, 0])
            cube([thickness + slop, thickness + slop, 2]);
        
        translate([-(thickness+slop)/2, -first_ring_diameter/2, 0])
            cube([thickness + slop, thickness + slop, 2]);
    }
}

second_ring_diameter = first_ring_diameter + thickness*2 + slop;
module second_ring() {
    cutout_radius = sin(45)*(second_ring_diameter/2);
    difference() {
        cylinder(d=second_ring_diameter, h=piece_height);
        cylinder(d=first_ring_diameter + slop, h=piece_height);
        
        #translate([-cutout_radius + thickness/2, cutout_radius - thickness/2, (piece_height-thickness)/2])
        rotate([0, 0, 45])
            cube([thickness + slop, thickness + slop, piece_height - thickness], center=true);
        
        #translate([cutout_radius - thickness/2, -cutout_radius + thickness/2, (piece_height-thickness)/2])
        rotate([0, 0, 45])
            cube([thickness + slop, thickness + slop, piece_height - thickness], center=true);
        
    }
    
    translate([second_ring_diameter/2 - thickness - thickness/2, 0, thickness/2])
        cube(thickness, center=true);
    
    translate([-second_ring_diameter/2 + thickness + thickness/2, 0, thickness/2])
        cube(thickness, center=true);
    
    translate([0, second_ring_diameter/2 - thickness - thickness/2, thickness/2])
        cube(thickness, center=true);
    
    translate([0, -second_ring_diameter/2 + thickness + thickness/2, thickness/2])
        cube(thickness, center=true);
}

third_ring_diameter = second_ring_diameter + thickness*2 + slop;
module third_ring() {
    difference() {
        union() {
            cylinder(d=third_ring_diameter, h=piece_height);
            
            difference() {
                union() {
                    // blade
                    translate([-third_ring_diameter/2 - thickness/2, -mount_area.y/2 - slop,  piece_height - antenna.z - thickness])
                        cube([120 + mount_area.y/2 + thickness/2, mount_area.y + slop*2, antenna.z+ thickness]);
                    
                    translate([120, 0, piece_height - antenna.z - thickness])
                        cylinder(h=antenna.z + thickness, d=mount_area.y + slop*2);
                }
                    
                // blade cavity
                translate([0, -antenna.y/2, piece_height - antenna.z - thickness])
                        cube([antenna.x + mount_area.x/2, antenna.y, antenna.z]);
                
                translate([120, 0, piece_height - antenna.z - thickness])
                        cylinder(h=antenna.z, d=magnet_diameter);
            }
        }
        cylinder(d=second_ring_diameter + slop, h=piece_height);
    }
    
    translate([0, (third_ring_diameter/2 - thickness - thickness/2)*-1, thickness/2])
        cube(thickness, center=true);
    
    translate([0, (third_ring_diameter/2 - thickness - thickness/2), thickness/2])
        cube(thickness, center=true);
}

module third_ring_left() {
    difference() {
        third_ring();
        
        difference() {
            translate([-mount_area.x/2, -mount_area.x/2, piece_height - antenna.z - thickness])
                cube([mount_area.x/2, mount_area.x/2, antenna.z + thickness]);
        
            translate([0, 0, piece_height - antenna.z - thickness])
                cylinder(h=antenna.z + thickness, d=mount_area.y + slop*2);
        }
    }
}

module third_ring_right() {
    difference() {
        third_ring();
        
        difference() {
            translate([-mount_area.x/2, 0, piece_height - antenna.z - thickness])
                cube([mount_area.x/2, mount_area.x/2, antenna.z + thickness]);
        
            translate([0, 0, piece_height - antenna.z - thickness])
                cylinder(h=antenna.z + thickness, d=mount_area.y + slop*2);
        }
    }
}